import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;


public class Robo extends Robot {

		Robo() throws AWTException,IOException{
			super();
		
		}
		public void delayMs(int ms){
			try{
				Thread.sleep(ms);
				}catch(Exception e){
					e.printStackTrace();
				}
		}
		public void mouseShift(int x,int y){
			this.mouseMove(x, y);
			this.delayMs(10);
		}
		
		public void mouseClick(int button){
			switch(button){
				case 1:
					this.mousePress(InputEvent.BUTTON1_MASK);
					this.delayMs(10);
					this.mouseRelease(InputEvent.BUTTON1_MASK);
				break;
				case 3:
					this.mousePress(InputEvent.BUTTON3_MASK);
					this.delayMs(10);
					this.mouseRelease(InputEvent.BUTTON3_MASK);
				break;
				
			}
			this.delayMs(10);
			
		}
		public void mouseDrag(int x1,int y1,int x2,int y2){
			this.mouseShift(x1, y1);
			this.mousePress(InputEvent.BUTTON1_MASK);
			for(int i=0;i<=y2-y1;i++){
				this.mouseShift(x1+i, y1+i);
			}
			this.mouseShift(x2, y2);
			this.mouseRelease(InputEvent.BUTTON1_MASK);
			this.delayMs(10);
		}
		
		public void copyText(){
			this.keyPress(KeyEvent.VK_CONTROL);
			this.keyPress(KeyEvent.VK_C);
			this.keyRelease(KeyEvent.VK_C);
			this.keyRelease(KeyEvent.VK_CONTROL);
			this.delayMs(10);
			
		}
		
		
		public void positionScreen(int dir,int mHeight,int x,int count){
			switch(dir){
			case -1:this.mouseShift(x, mHeight-20);
					this.delayMs(10);
					this.mousePress(InputEvent.BUTTON1_MASK);
					for(int i=mHeight-20;i>=mHeight/2;i--){
							this.delayMs(10);
							this.mouseMove(x,i);
						}
						this.mouseRelease(InputEvent.BUTTON1_MASK);
						this.delayMs(10);
				break;
			case 1:
				for(int j=1;j<=count;j++){
		
						this.mouseShift(x, mHeight/2);
						this.delayMs(10);
						this.mousePress(InputEvent.BUTTON1_MASK);
						
						for(int i=mHeight/2;i<=mHeight-20;i++){
							this.delayMs(10);
							this.mouseMove(x,i);
						}
						this.mouseRelease(InputEvent.BUTTON1_MASK);
						this.delayMs(10);
					}	
				break;
		}
	}
		
		public void minimize(int maxWidth)
		{
			this.mouseShift(maxWidth-80,1);
			this.mouseClick(1);
		}
		
		public String toString(){
			return "Chitti speed:1000MHz memory:1ZB";
		}
}
